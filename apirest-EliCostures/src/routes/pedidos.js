const express = require("express");
const router = express.Router();

const dbpath = ("../database.js")
const mysqlConnection = require(dbpath);

const bodyParser = require('body-parser')

router.use(bodyParser.urlencoded({ extended: false }))

router.post('/pedidos', (req, res) => {

    const nombre = req.body.nombre
    const telefono = req.body.telefono
    const correo = req.body.correo
    const comuna = req.body.comunaselect
    const comentario = req.body.comentario
    const ajuste_entalle = req.body.ajuste_entalle
    const basta = req.body.basta
    const reduccion = req.body.reduccion
    const cambio_cierre = req.body.cambio_cierre
    const entubado_pantalon = req.body.entubado_pantalon

    //res.end();

    mysqlConnection.query("INSERT INTO pedidos (nombre, telefono, correo_electronico, comuna, ajuste_o_entalle, basta_vestido_falda_pantalon, reduccion_de_prenda, cambio_cierre, entubado_pantalon, comentario, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [nombre, telefono, correo, comuna, ajuste_entalle, basta, reduccion, cambio_cierre, entubado_pantalon, comentario, 1], (err, rows, fields) => {

        let data = {};
        if (!err) {
            data = {
                "result": 'Ok'
            }
            res.json(rows);

        } else {

            data = {
                "result": 'No Ok',
                "code": 0,
                "errors": [{
                    "userMessage": "No se pudo actualizar",
                    "internalMessage": "ocurrio un error al procesar en base de datos",
                }],
                "body": req.body
            }
            console.log("la data 1" + data)
            res.status(500).json(data)
            res.end()
        }
    });
});

router.get('/pedidos-detalle/:numero_orden', (req, res) => {

    console.log(req.params.numero_orden)
    const idpedido = req.params.numero_orden
        //res.end();



    mysqlConnection.query("SELECT p.idpedido, p.nombre, e.descripcion_estado FROM pedidos p INNER JOIN estado e WHERE e.id_estado = p.estado and p.idpedido = ?", [idpedido], (err, rows, fields) => {

        let data = {};
        if (!err) {
            data = {
                "result": 'Ok'
            }
            res.send(rows)
            res.status(200);
            res.end()

        } else {

            data = {
                "result": 'No Ok',
                "code": 0,
                "errors": [{
                    "userMessage": "No se pudo actualizar",
                    "internalMessage": "ocurrio un error al procesar en base de datos",
                }],
            }
            res.status(500).json(data)
            res.end()
        }
    });
});

module.exports = router;