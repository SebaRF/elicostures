//Urls de la api para GET y POST
var url = 'https://api-elicostures-nodejs.uc.r.appspot.com';
var urlDetalle = 'https://api-elicostures-nodejs.uc.r.appspot.com/pedidos-detalle/';

/////////////////////////////////////////////////////////////////////////////

//Trae data de comunas y las lista en el select de formulario pedido 
fetch(url)

.then(response => response.json())
.then(data => {
  console.log(data)


  var select = document.getElementById('comunasavailable');
if (select !=null){
  for(var i=0; i < data.length; i++){ 
        var option = document.createElement("option"); //Creamos la opcion
        option.innerHTML = data[i].comuna; //Metemos el texto en la opción
        select.appendChild(option); //Metemos la opción en el select
    }
}
})

/////////////////////////////////////////////////////////////////////////////

// Formularios de las diferentes páginas
var formularioIndex = document.getElementById('formulario');
var formularioPedidos = document.getElementById('formulario-detalle');

/////////////////////////////////////////////////////////////////////////////

//Verifica si se completó el formulario, si si hará POST para registrar pedido
if (formularioIndex != null){
  console.log("Hay formulario")
    formularioIndex.addEventListener('submit', async function (e) {
      e.preventDefault();
      const formData = new FormData(formularioIndex).entries()
      const response = await fetch('https://api-elicostures-nodejs.uc.r.appspot.com/pedidos', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(Object.fromEntries(formData))
      });

      const result = await response.json();
      console.log(result.insertId)
      console.log(response)

      if (response.status == 200){
        $('#exampleModal').modal('show'); 
        var position = document.getElementById("numero_orden")
        var orden = document.createTextNode('000'+result.insertId)
        var clean = document.createTextNode('')
        position.appendChild(orden)
        document.getElementById("exampleModal").style.display="visible";

      }else{
        $('#exampleModalError').modal('show'); 
      }
  })
} 




function detallePedido(){
  // Selecting the input element and get its value 
  var inputVal = document.getElementById("myInput").value;

  if (inputVal === ""){
     inputVal = 0;
   }
  // Displaying the value

  fetch(urlDetalle+inputVal)

  .then(response => response.json())
  .then(dataPedido => {
    console.log(dataPedido);
    var large = dataPedido.length;

    if (large > 0){

        var divDetalle = document.getElementById("resultado-detalle");

        var estadoOrden = document.createElement("p"); //Creamos la opcion
        var estadoNombre = document.createElement("p"); //Creamos la opcion
        var estadoPedido = document.createElement("p"); //Creamos la opcion

        estadoOrden.innerHTML = "Orden: "+dataPedido[0].idpedido; //Metemos el texto en la opción
        estadoNombre.innerHTML = "Cliente: "+dataPedido[0].nombre; //Metemos el texto en la opción
        estadoPedido.innerHTML = "Estado: "+dataPedido[0].descripcion_estado; //Metemos el texto en la opción
        

        console.log(dataPedido[0].idpedido);
        console.log(dataPedido[0].nombre);
        console.log(dataPedido[0].descripcion_estado);
        // divDetalleText = "Pedido: ";
        // divDetalleText + 
        divDetalle.appendChild(estadoOrden); //Metemos la opción en el select
        divDetalle.appendChild(estadoNombre); //Metemos la opción en el select
        divDetalle.appendChild(estadoPedido); //Metemos la opción en el select

    }
      else{

        $('#exampleModalError').modal('show'); 

    }


  })

}

// Oculta menu al seleccionar algún sub-menu (x3)

document.querySelector("#inicio_").addEventListener('touchstart', fa);
function fa(evi){ 

  console.log('evi: '+evi.touches);
  $('.navbar-collapse').collapse('hide');
}

document.querySelector("#servicio_").addEventListener('touchstart', fb);
function fb(evs){ 

  console.log('evs: '+evs.touches);
  $('.navbar-collapse').collapse('hide');
}

document.querySelector("#contacto_").addEventListener('touchstart', fc);
function fc(evc){ 

  console.log('evc: '+evc.touches);
  $('.navbar-collapse').collapse('hide');
}
//////////////////////////////////////////////////////////////


//Esconde navbar al hacer scroll

var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("navbar").style.top = "0";
  } else {
    document.getElementById("navbar").style.top = "-100px";
  }
  prevScrollpos = currentScrollPos;
}
//////////////////////////////////////////////////////////////


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function isCero(id) {
    elemento = document.getElementById(id);
    console.log(elemento.value);
    if (elemento.value === ""){
      elemento.value = 0;
      return
    }
    
}

function cleanPedido(idclean) {

  var elementClean = document.getElementById(idclean);
  elementClean.innerHTML = "";

}


